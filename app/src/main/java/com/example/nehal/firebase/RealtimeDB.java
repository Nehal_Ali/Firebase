package com.example.nehal.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RealtimeDB extends AppCompatActivity {
    //Do not forget to set default fields and values in Firebase Console Realtime Database
    Button saveBtn, retrieveBtn;
    TextView nameTxt, ageTxt, msgTxt;
    EditText nameEdit, ageEdit, msgEdit;

    //define instance of Firebase DB
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_db);
        saveBtn = (Button) findViewById(R.id.save);
        retrieveBtn = (Button) findViewById(R.id.retrieve);
        nameTxt = (TextView) findViewById(R.id.nametxt);
        ageTxt = (TextView) findViewById(R.id.agetxt);
        msgTxt = (TextView) findViewById(R.id.msgtxt);
        nameEdit = (EditText) findViewById(R.id.editText3);
        ageEdit = (EditText) findViewById(R.id.editText4);
        msgEdit = (EditText) findViewById(R.id.editText5);


/////////// Initialize Firebase Instance with reference
        //reference extracts variables names and sets fields in database with same name
        //and value entered
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //go to User.java Class for more info
       saveBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
//          retrieve from edit texts to write to Database
               String name = nameEdit.getText().toString();
               String message = msgEdit.getText().toString();
               int age = Integer.parseInt(ageEdit.getText().toString());
               //initiazlise a class object with values passed to constructor
                User user = new User(name, age, message);
                //set values to database reference
                mDatabase.setValue(user);
           }
       });

       retrieveBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mDatabase.addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       User user = dataSnapshot.getValue(User.class);
                       String name = user.getName().toString();
                       String message = user.getMessage().toString();
                       int age = user.getAge();
                        //since this is a realtime database and onDatachange fa textviews
                       //are updated frequently 3la toul, no need for the onclick listener

                       //You have to use 'String.valueOf()' because otherwise the app crashes
                       nameTxt.setText(String.valueOf(name));
                       ageTxt.setText(String.valueOf(age));
                       msgTxt.setText(String.valueOf(message));

                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }


               });
           }
       });

     }
}
