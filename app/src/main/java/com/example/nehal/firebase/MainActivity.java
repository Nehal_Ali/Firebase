package com.example.nehal.firebase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    Button signUpBtn, loginBtn, realtimeDB, storage;
    EditText emailText, passwordText;
    CheckBox rememberMe;
//to handle back button press in android
    boolean doubleClick = false;
    String email, password;
    //firebase authentication instance
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signUpBtn = (Button) findViewById(R.id.button);
        loginBtn = (Button) findViewById(R.id.button2);
        emailText = (EditText) findViewById(R.id.editText);
        passwordText = (EditText) findViewById(R.id.editText2);
        rememberMe = (CheckBox) findViewById(R.id.checkBox);
        realtimeDB = (Button) findViewById(R.id.Realtime);
        storage = (Button) findViewById(R.id.storage);

        //initializing firebase authentication instance
        mAuth = FirebaseAuth.getInstance();

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = emailText.getText().toString();
                password = passwordText.getText().toString();

                //check if fields are not empty
                if (!email.equals(null) && !password.equals(null)){
                    //standard authentication; creating a user with email and password entered
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("EmailPassword", "createUserWithEmail:success");
                                        if(rememberMe.isChecked()){
                                            //shared preferences to save a boolean named Remember Me for checkBox
                                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                            SharedPreferences.Editor edit = prefs.edit();
                                            edit.putBoolean("Remember Me", Boolean.TRUE);
                                            edit.commit();
                                        }
                                        startActivity(new Intent(MainActivity.this, Main2Activity.class));
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w("EmailPassword", "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(getBaseContext(), "Authentication failed. Password too short!",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                }
                            });
                } else {
                    Toast.makeText(getBaseContext(), "Please Fill All Fields", Toast.LENGTH_SHORT).show();
                }

            }
        });

//login authentication
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //retrieve the email and passwords into strings from EditTexts
                email = emailText.getText().toString();
                password = passwordText.getText().toString();

                if (email!=null && password!=null){
                    //Use Firebase Authentication to check Login; standard to login by authentication
                    mAuth.signInWithEmailAndPassword(email,password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if(task.isSuccessful()) {
                                        Intent i = new Intent(MainActivity.this, Main2Activity.class);

                                        //to set feature to stay signed in, create a boolean shared preferences
                                        if (rememberMe.isChecked()) {
                                            //if checkBox is check, save a boolean to true in shared preferences

                                            Toast.makeText(getBaseContext(), "Checked", Toast.LENGTH_SHORT).show();

                                        }
                                        else {
                                            Toast.makeText(getBaseContext(), "NOT Checked", Toast.LENGTH_SHORT).show();
                                        }

                                        startActivity(i);
                                    }
                                    else
                                    {

                                        Toast.makeText(getBaseContext(),""+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        Log.d("EmailPassword",task.getException().getMessage());
                                    }
                                }
                            });
                }
                else{
                    Toast.makeText(getBaseContext(),"Please Enter Email & Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
//open realtime database activity
        realtimeDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RealtimeDB.class));
                finish();
            }
        });

        //open storage activity
        storage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Storage.class));
                finish();
            }
        });

    }
}
