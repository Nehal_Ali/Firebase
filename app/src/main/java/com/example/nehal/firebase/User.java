package com.example.nehal.firebase;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
//      this class helps define data to retrrieve and save them with constructor
    //the constructor is instead of setters and getters
    public String name;
    public String message;
    public int age;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String name,int age, String message) {
        this.name = name;
        this.age = age;
        this.message = message;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}