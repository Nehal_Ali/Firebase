package com.example.nehal.firebase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

public class Main2Activity extends AppCompatActivity  {
    Button refreshBtn;
    TextView  DownstreamTxt;
    boolean doubleClick = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        refreshBtn = (Button) findViewById(R.id.button3);
        DownstreamTxt = (TextView) findViewById(R.id.textView);
//to send a notification (downstream message via firebase) you have to go to Firebase console
        //and enter Cloud Messaging section then choose to create new message and add message body
        // also add your project as receiver or token ID for single device
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("Message","");
        edit.commit();

        //retrieve value of Checkbox from shared preferences
        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean keepSignedIn = prefs2.getBoolean("Remember Me", false);
        if(!keepSignedIn) { //if value found to be false, then start the login activity, otherwise continue with current
            Toast.makeText(getBaseContext(),"Not Checked!",Toast.LENGTH_SHORT).show();
        }
        else{
            Log.d("REMEMBER", "Remember Me: "+keepSignedIn);
        }

refreshBtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        //retrieve value of message from shared preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String message = prefs.getString("Message", null);
        if(message!=null){
            DownstreamTxt.setText(message);
        }
        else{
            Log.d("TAG", "NO Message");
        }
    }
});



    }

    //on back pressed double times within interval to close app
    @Override
    public void onBackPressed() {
        if (doubleClick) {
            super.onBackPressed();
            finish();
            System.exit(0);
            return;
        }

        this.doubleClick = true;
        Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleClick = false;
            }
        }, 2000);
    }

}
